# Exercícios Semanas 1 e 2

## 1 Construa um index

Construa um documento HTML5, chamado “index.html”, em que a linguagem seja português do Brasil, o conjunto de caracteres seja UTF-8, e que o conteúdo preencha toda a área do navegador, com o título na aba de “DEV in House”. O corpo deve conter um título (h1) com o conteúdo “Semana 1” e um parágrafo contendo o texto “Este é o resultado do primeiro exercício da primeira semana do curso DEVinHouse.”. No mesmo documento, adicione uma linha horizontal entre o título e o parágrafo, e duas quebras de linha no parágrafo, uma logo após a palavra “exercício” e outra logo após a palavra “semana”. No mesmo documento, insira algum emoji de sua escolha no parágrafo.

## 2 Imagens

Construa outro documento HTML5, no mesmo diretório do anterior, agora contendo 3 imagens: uma salva no mesmo diretório dos arquivos .html, outra salva em um diretório diferente, e outra que não está no seu computador, mas está salva em algum servidor na internet. No mesmo documento, mude o “ícone de favorito” da página (o ícone da aba). Faça o mesmo para o documento anterior.

## 3 Estilos

No index, do exercício 1, adicione um arquivo css e mude o estilo do título de nível 1 para que a fonte seja Verdana, tamanho 20, cor marrom. Nesse mesmo documento, utilizando CSS: centralize o título, mude a cor de fundo da página para amarelo e mude o tamanho da fonte dos parágrafos para 14

## 4 Botões

Adicione um botão contendo o texto “Avançar” e, utilizando apenas HTML, faça com que o usuário, ao clicar nesse botão, vá para a página das imagens. Substitua o texto “Avançar” do botão do por uma imagem. Faça com que o botão tenha uma borda curvada. Aumente o padding do botão em 10 pixels, coloque uma borda sólida de 2 pixels na cor preta, e uma margem superior de 20 pixels.

## 5 Vídeos

Incorpore um vídeo do Youtube na página index, e adicione, acima do player, um título e uma descrição sobre o mesmo.

## 6 Alerte o Usuário

Utilizando JavaScript, emita um alerta para o usuário com alguma informação importante.

## 7 Faça com que o usuário confirme antes de seguir

Utilizando JavaScript, peça a confirmação do usuário ao clicar em algum botão, perguntando se ele deseja realmente realizar aquela ação (você pode utilizar a página criada nos exercícios da semana 1, ou criar uma nova).

## 8 Peça o nome do usuário e exiba na tela

Faça com que o navegador do usuário pergunte o nome dele(a). Utilizando o método alert, exiba o nome inserido pelo usuário.
