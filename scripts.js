alert("Bem vindo a página de fantasia medieval.");
prompt(); // Tive que add essa linha pois estava indo direto para o 2º alert
let userName = prompt("Qual o seu nome?");

alert(`Seja muito bem vindo(a) ${userName}`);

const btn = document.querySelector(".btn");
const link = btn.getAttribute("href");

const userConfirm = () => {
  let confirmation = confirm("Quer realmente mudar de página?");

  if (confirmation) {
    window.open(link, "_self");
  }
};
